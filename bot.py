import os

import discord
from dotenv import load_dotenv
import re

import aiohttp



load_dotenv()
SERVER = os.getenv('DISCORD_GUILD')
TOKEN = os.getenv('DISCORD_TOKEN')

class WatchroomLinkBot(discord.Client):

    knownCommands = ["purge", "purgeall", "help", "w2g"]
    roomPatterns = [".*w2g\.tv.*", ".*crunchyroll\.com.*rollTogetherRoom.*"]

    def __init__(self):

        intents = discord.Intents.default()
        intents.members = True
        super().__init__(intents=intents)

    async def on_ready(self):

        for guild in client.guilds:
            if guild.name == SERVER:
                break

        self.me = client.user
        self.guild = guild
        print(f'{client.user} has connected to Discord!')
        print(f'{guild.name}(id: {guild.id})')

        members = '\n - '.join([member.name for member in guild.members])
        print(f'Guild Members:\n - {members}')

    def isCommand(slef, message):

        return message.clean_content.strip()[:4] == "-wlb"


    def isRoomLink(self, message):

        for pattern in WatchroomLinkBot.roomPatterns:

            if re.match(pattern, message.clean_content):

                return True

        return False

    async def executeCommand(self, commandChain, message):


        if len(commandChain) < 2:

            await message.reply("Beep Boop I don't understand.\nType \"-wlb help\" for understood commands.")
            return

        baseCommand = commandChain[1]
        todos = []

        if baseCommand not in WatchroomLinkBot.knownCommands:

            await message.reply("Beep Boop I don't understand.\nType \"-wlb help\" for understood commands.")
            return 

        await message.add_reaction("👍")


        if baseCommand == "purgeall":

            for oldMessage in await message.channel.history(limit=200).flatten():

                if self.isCommand(oldMessage) or self.isRoomLink(oldMessage) or oldMessage.author == self.me:

                    todos.append(oldMessage.delete())
        
        elif baseCommand == "purge":

            for oldMessage in await message.channel.history(limit=200).flatten():

                if self.isRoomLink(oldMessage):

                    todos.append(oldMessage.delete())

        elif baseCommand == "help":

            response = "I understand the following commands :)\n\n"
            for command in WatchroomLinkBot.knownCommands:
                response += "\t- " + command + "\n"
            todos.append(message.reply(response))

        elif baseCommand == "w2g":

            async with aiohttp.ClientSession() as session:
                
                url = "https://w2g.tv/rooms/create"
                    
                async with session.post(url) as resp:
                    
                    await message.channel.send("Here is your watch2gether url: " + str(resp.url))

        for todo in todos:

            await todo

    async def on_message(self, message):

        if not self.isCommand(message):

            return
        
        print("Recieved command:", message.clean_content)
        commandChain = message.clean_content.strip().split(" ")
        commandChain = list(filter(lambda e: e != "", commandChain))
        
        await self.executeCommand(commandChain, message)

client = WatchroomLinkBot()
client.run(TOKEN)